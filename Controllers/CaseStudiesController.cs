﻿using System;
using System.Collections.Generic;
using Mandee.Models;
using Microsoft.AspNetCore.Mvc;

namespace Mandee.Controllers
{
    public class CaseStudiesController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            /*
            * Assign page title with view bag
            * Make a AdModel list and pass to view
            */
            ViewBag.title = "CaseStudies";
            List<Models.AdModel> modelList = Domain.ContentCreation.CreationUtilties.MakeCaseStudies();
            return View(modelList);
        }

        [HttpGet]
        public IActionResult CaseStudy(AdModel ad)
        {
            /*
            * Assign page title with view bag
            */
            ViewBag.title = "Case study";
            return View(ad);
        }

        [HttpGet]
        public IActionResult FindOutMore(string Title, string Author, string Description, string EndAd, string startAd, string url)
        {
            /*
            * Assign page title with view bag
            * Create a model using variables passed to pass model to view
            */
            ViewBag.title = "Find out more";
            var model = new AdModel(){Title= Title, Author = Author, Description = Description, EndAd = new DateTime(), startAd = new DateTime(), Url = url};
            return RedirectToAction("CaseStudy", model);
        }
    }
}