﻿using System.Web;
using Microsoft.AspNetCore.Mvc;
using Mandee.Models;

namespace Mandee.Controllers
{
    public class InterviewController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            /*
            * Assign page title with view bag
            */
            ViewBag.title = "Interview request";
            return View();
        }

        [HttpGet]
        public IActionResult InterviewForm()
        {
            /*
            * Assign page title with view bag
            */
            ViewBag.title = "Interview request";
            return View();
        }

        [HttpPost]
        public IActionResult InterviewForm(VacancyApplyModel vacancy)
        {
            /*
            * Assign page title with view bag
            * Check model is correct or act accordingly
            */
            ViewBag.title = "Interview request";
            if (ModelState.IsValid)
            {
                ViewBag.title = "Interview form";
                return View("ThankYou", vacancy);
            }
            else
            {
                return View();
            }
        }

        [HttpGet]
        public IActionResult ThankYou()
        {
            /*
            * Assign page title with view bag
            */
            ViewBag.title = "Thankyou";
            return View();
        }
    }
}