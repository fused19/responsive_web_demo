﻿using System.Collections.Generic;
using Mandee.Models;
using Microsoft.AspNetCore.Mvc;

namespace Mandee.Controllers
{
    public class BlogController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            /*
            * Assign page title with view bag, create a model and pass to view
            */
            ViewBag.title = "Blog";
            List<BlogModel> modelList = Domain.ContentCreation.CreationUtilties.MakeBlogPosts();
            return View(modelList);
        }
    }
}