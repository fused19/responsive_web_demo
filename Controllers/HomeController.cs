﻿using System.Collections.Generic;
using Mandee.Models;
using Microsoft.AspNetCore.Http;  
using Microsoft.AspNetCore.Mvc; 

namespace Mandee.Controllers
{
	public class HomeController : Controller
	{
		const string LoggedUserID = "_Logged";
		const string UserID = "_ID";

		[HttpGet]
	    public IActionResult Index()
		{
		    /*
            * Assign page title with view bag, create a model and pass to view
            */
		    ViewBag.title = "Welcome";
		    List<Models.AdModel> modelList = Domain.ContentCreation.CreationUtilties.MakeCaseStudies();
		    return View(modelList);
		}

	    [HttpGet]
	    public IActionResult RegistrationForm()
	    {
	        /*
            * Assign page title with view bag
            */
	        ViewBag.title = "Registration";
	        return View();
	    }

	    [HttpPost]
	    public IActionResult RegistrationForm(RegModel registration)
	    {
            /*
            * Assign page title with view bag, check returned model is valid
            * if true pass model to completed view if false return to view
            */
	        ViewBag.title = "Registration required";
	        if (ModelState.IsValid)
	        {
	            return View("Completed", registration);
	        }
	        else
	        {
	            return View();
	        }
	    }

	    [HttpGet]
	    public IActionResult LoginForm()
	    {
	        /*
            * Assign page title with view bag
            */
	        ViewBag.title = "Login";
	        return View();
	    }

	    [HttpPost]
	    [ValidateAntiForgeryToken]
	    public IActionResult LoginForm(UserModel user)
	    {
	        /*
            * Check if model properties are set and then set session variable.
            * Allow session status to be passed between pages using tempData
            * Assign page title with view bag, check returned model is valid
            * if true pass model to secure view if false return to view
            */
	        ViewBag.title = "Login";
	        if (ModelState.IsValid)
	        {
	            if (!((user.Password != null) & (user.Email != null))) return View(user);
	            var UserList = new UserModel(){Password = "pass", Email = "demo@demo.com"};
				HttpContext.Session.SetString(LoggedUserID, UserList.Email.ToString());
	            TempData["UserID"] = HttpContext.Session.GetString(LoggedUserID);
	            //return RedirectToAction("Secure");
	            return Redirect(Url.RouteUrl(new { controller = "Home", action = "Secure" }) + "#anchor");
	        }
	        else
	        {
	            return View();
	        }
	    }

	    [HttpGet]
	    public IActionResult Completed()
	    {
	        /*
            * Assign page title with view bag
            */
	        ViewBag.title = "Thank you";
	        return View();
	    }

	    [HttpGet]
	    public IActionResult Secure()
	    {
	        /*
            * Assign page title with view bag
            * Check session status and return view
            * If navigate away then null TempData and force a re-login
            */
	        ViewBag.title = "Restricted Area";
	        if (HttpContext.Session.GetString(LoggedUserID) != null && TempData["UserID"] != null )
	        {
	            return View();
	        }
	        TempData["UserID"] = null;
	        return RedirectToAction("LoginForm", "Home");
	    }

	    [HttpGet]
	        /*
            * create a model and pass to partial model
            */
	    public IActionResult TeamPartial()
	    {
	        List<TeamModel> modelList = Domain.ContentCreation.CreationUtilties.MakeTeam();
	        return PartialView("Shared/_TeamPartial",modelList);
	    }
	}
}
