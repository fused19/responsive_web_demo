﻿using System;
using System.Collections.Generic;
using Mandee.Models;

namespace Mandee.Domain.ContentCreation
{
    public class CreationUtilties
    {
        /// <summary>
        /// Returns a list of job CaseStudies in the form of AdModel objects
        /// </summary>
        public static List<AdModel> MakeCaseStudies()
        {
            string[] title = {"Kon’nichiwa Azuma","How we roll...","Red Hot Awards 2017"};
            string[] description =
            {
                "A film about new trains",
                "Culture changing film",
                "A film about our annual employee award"
            };
            string[] url = {@"https://bit.ly/2LUnrqH",@"https://bit.ly/2l9K3rZ",@"https://bit.ly/2JDsnno"};
            DateTime[] dates = {DateTime.UtcNow.AddDays(2), DateTime.UtcNow.AddHours(6), DateTime.UtcNow.AddMonths(2)};
            DateTime[] dates2 = {DateTime.UtcNow.AddHours(6), DateTime.UtcNow.AddDays(10) , DateTime.UtcNow.AddMonths(3)};
            string[] Authors = {"Amanda Richmond","Mandee R","Amanda R" };
            List<AdModel> modelList = new List<AdModel>();
            for (var i = 0; i < 3; i++)
            {
                var model = new AdModel(){Title = title[i], Description = description[i], Author = Authors[i], EndAd = dates[i], startAd = dates2[i], Url = url[i]};
                modelList.Add(model);
            }
            return modelList;
        }

        /// <summary>
        /// Returns a list of blog posts in the form of BlogModel objects
        /// </summary>
        public static List<BlogModel> MakeBlogPosts()
        {

            string[] Title = {"Finding the right candidate","What can you do?","The power of a question"};
            string[] Description =
            {
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sodales in ipsum non pellentesque. Quisque luctus elit vitae faucibus vestibulum. Nunc malesuada, elit a hendrerit.",
                "Morbi id libero lacinia, iaculis metus et, rhoncus sem. Etiam enim leo, efficitur in mauris at, finibus ultricies enim. Sed mattis ante vitae accumsan pellentesque.",
                "Etiam vel libero a neque commodo vulputate. Nulla quis nisi quis orci placerat tincidunt. Nunc rutrum leo ut urna hendrerit, ut faucibus justo maximus. Aliquam."
            };
            string[] BLine = { "When they're right in front of you","The interview survival guide","Or how to catch a winner"};
            DateTime[] dates = {DateTime.UtcNow.AddDays(2), DateTime.UtcNow.AddHours(6), DateTime.UtcNow.AddMonths(2)};
            string[] Authors = {"Toni Shuh","Super Hans","Mary Poppins" };
            List<BlogModel> modelList = new List<BlogModel>();
            for (var i = 0; i < 3; i++)
            {
                var model = new BlogModel(){Title = Title[i], BodyCopy = Description[i], Author = Authors[i], PublishedDate = dates[i], ByLine = BLine[i]};
                modelList.Add(model);
            }
            return modelList;
        }

        /// <summary>
        /// Generates a user
        /// </summary>
        public static Models.UserModel MakeUser()
        {
            var model = new Models.UserModel();
            return model;
        }

        /// <summary>
        /// Returns a list of Team members in the form of TeamModel objects
        /// </summary>
        public static  List<TeamModel> MakeTeam()
        {
            string[] Name = {"Randy Daniels","Si Lan", "Lola Von Schmitt"};

            string[] Role = {"Explorer of Dreams","Smiles Administer","Commander in Chief"};

            string[] Pic = {"pic1.jpg","pic2.jpg","pic3.jpg"};

            string[] Comment = {"Rub my tummy and hear me prrrr, hmmmm.", "Long walks in the country, grrreeeat.","Ice cream and hugs are my passion."};

            List<TeamModel> modelList = new List<TeamModel>();
            for (var i = 0; i < 3; i++)
            {
                var model = new TeamModel(){Name = Name[i], Role = Role[i], Pic = Pic[i], Comment = Comment[i]};
                modelList.Add(model);
            }
            return modelList;
        }
    }
}