﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Mandee.Models
{
    public class RegModel
    {
        [DisplayName("Name:")]
        [Required(ErrorMessage="A name is required")]
        public string Name { get; set; }
        [DisplayName("Email:")]
        [Required(ErrorMessage="A vaild email address is needed")]
        [RegularExpression(".+\\@.+\\..+",ErrorMessage="All email addresses have an @ symbol")]
        public string Email { get; set; }
        [DisplayName("Phone Number:")]
        [Required(ErrorMessage="Please provide a contact number")]
        public string Phone { get; set; }
        [DisplayName("Address:")]
        [Required(ErrorMessage="Please provide a contact address")]
        public string Address { get; set; }
    }
}