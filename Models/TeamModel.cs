﻿namespace Mandee.Models
{
    public class TeamModel
    {
        public string Name { get; set; }

        public string Role { get; set; }

        public string Pic { get; set; }

        public string Comment { get; set; }
    }
}